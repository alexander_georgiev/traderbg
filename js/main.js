/* === Define functions and variables === */
var formIsValid = false,
mailValid 	= false,
passValid 	= false,
$submitBtn  = $('.btn-submit'),
$inputEmail = $('#email'),
$inputPass  = $('#password'),
$bodyTag    = $('body'),
/* === email regex check - returns true / false === */
isValidEmailAddress = function (email) {
	var regEx = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	if(regEx.test(email)) {
		mailValid = true;
		//alert("Mail is valid!");
		//console.log("MAIL: " + mailValid);
	}else {
		//alert("Mail is invalid;");
		mailValid = false;
		//console.log("MAIL: " + mailValid);
	}
},
/* === resetForm is initialized only if the form is valid === */
resetForm = function () {
	$submitBtn.removeClass('disabled');
	$inputEmail.val('');
	$inputPass.val('');
	$('.form-item').removeClass('invalid'),
	setTimeout(function() {
		$submitBtn.addClass('disabled');
		$bodyTag.removeClass('modal-open');
	}, 400);
}
/* === checkValid is initialized on the submit button click event === */
checkValid = function (emailToCheck) {
	isValidEmailAddress(emailToCheck);
	//console.log(emailToCheck + ' and ' + passToCheck);
	if($inputPass.val().length > 4){
		passValid = true;
		//alert("Password is valid!");
		//console.log("PASSWORD: " + passValid);

	}else{
		passValid = false;
		//alert("Password is invalid!");
		//console.log("PASSWORD: " + passValid);
	}
	
	if (mailValid && passValid) {
		formIsValid = true;

		resetForm();
		//console.log("FORM VALID!");
	}else {
		$submitBtn.addClass('disabled');
		$('.form-item').addClass('invalid').click(function(event) {
			$(this).removeClass('invalid');
		});
		//console.log("FORM INVALID!");
	};
};

/* === open/close modal and validate form === */
jQuery(document).ready(function($) {
	$('.free-app').click(function(event) {
		event.preventDefault();
		$bodyTag.addClass('modal-open');
	});
	$('.close').click(function(event) {
		event.preventDefault();
		resetForm();
	});
	$($submitBtn).click(function(event) {
		event.preventDefault();
		var email = $inputEmail.val();
		checkValid(email);
	});
});

